use crate::imports::*;

#[repr(C)]
pub struct BluetoothSettingsWidget {
    pub parent: gtk::ffi::GtkBox,
}

#[repr(C)]
pub struct BluetoothSettingsWidgetClass {
    pub parent_class: gtk::ffi::GtkBoxClass,
}

extern "C" {
    fn bluetooth_settings_widget_new() -> *mut BluetoothSettingsWidget;
    fn bluetooth_settings_widget_get_type() -> glib::ffi::GType;

    fn bluetooth_settings_widget_get_default_adapter_powered(this: *mut BluetoothSettingsWidget) -> bool;
    fn bluetooth_settings_widget_set_default_adapter_powered(this: *mut BluetoothSettingsWidget, state: bool);
}
