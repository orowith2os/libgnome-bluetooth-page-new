pub mod imports {
pub use gtk4 as gtk;
pub use gtk::prelude::*;
pub use gtk::glib;
pub use gtk::subclass::prelude::*;
pub use crate::glib::{Object, Type, closure_local, subclass::Signal};
pub use std::sync::OnceLock;
}
use imports::*;

pub mod ffi;
pub mod imp;

glib::wrapper! {
    pub struct BluetoothSettingsWidget(ObjectSubclass<imp::BluetoothSettingsWidget>)
        @extends gtk::Box, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl BluetoothSettingsWidget {
    pub fn new() -> Self {
        Object::builder::<BluetoothSettingsWidget>().build()
    }

    pub fn get_default_adaper_powered(&self) -> bool {
        self.imp().get_state()
    }

    pub fn set_default_adapter_powered(&self, input: bool) {
        self.imp().set_state(input);
    }
}

