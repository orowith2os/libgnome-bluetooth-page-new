use crate::imports::*;
use std::cell::Cell;

#[derive(Default)]
pub struct BluetoothSettingsWidget {
    state: Cell<bool>,
}

impl BluetoothSettingsWidget {
    pub fn get_state(&self) -> bool {
        let x = self.state.get();
        println!("get_state called, current state is: {}", x);
        x
    }

    pub fn set_state(&self, input: bool) {
        self.state.set(input);
        println!("set_state called, new state is: {}", input);
    }
}

#[glib::object_subclass]
impl ObjectSubclass for BluetoothSettingsWidget {
    const NAME: &'static str = "BluetoothSettingsWidget";
    type Type = super::BluetoothSettingsWidget;
    type ParentType = gtk::Box;
}

impl ObjectImpl for BluetoothSettingsWidget {
    fn signals() -> &'static [Signal] {
        static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
        SIGNALS.get_or_init(|| {
            vec![Signal::builder("panel-changed").build(),
                 Signal::builder("adapter-status-changed").build()]
        })
    } 

    fn constructed(&self) {
        self.parent_constructed();
        let image = gtk::Image::from_icon_name("bluetooth-symbolic");
        let obj = self.obj();
        obj.append(&image);
    }
}
impl WidgetImpl for BluetoothSettingsWidget {}
impl BoxImpl for BluetoothSettingsWidget {}

pub(crate) mod ffi {
    use super::*;
    use glib::translate::*;

    pub type BluetoothSettingsWidget = <super::BluetoothSettingsWidget as super::ObjectSubclass>::Instance;

    #[no_mangle]
    pub extern "C" fn bluetooth_settings_widget_get_default_adapter_powered(this: *mut BluetoothSettingsWidget) -> bool {
        unsafe { &*this }.imp().get_state()
    }

    #[no_mangle]
    pub extern "C" fn bluetooth_settings_widget_set_default_adapter_powered(this: *mut BluetoothSettingsWidget, state: bool) {
        unsafe { &*this }.imp().set_state(state); 
    }

    #[no_mangle]
    pub extern "C" fn bluetooth_settings_widget_new() -> *mut BluetoothSettingsWidget {
        let obj = glib::Object::builder::<super::super::BluetoothSettingsWidget>()
            .build();
        obj.to_glib_full()
    }

    #[no_mangle]
    pub extern "C" fn bluetooth_settings_widget_get_type() -> glib::ffi::GType {
        let _ = gtk::init();
        <super::super::BluetoothSettingsWidget as StaticType>::static_type().into_glib()
    }
}
